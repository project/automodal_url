
(function ($) {

Drupal.behaviors.autoModalURL = function(context) {

  $('.automodal:not(.automodal-url-processed)').each(function() {
    if ($(this).attr('href') == Drupal.settings.automodal_url['url']) {
      $(this).trigger('click');
    }
  }).addClass('automodal-url-processed');

};

})(jQuery);
