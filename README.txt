
Summary
-------
Automodal URL will allow you to pass a path in your URL to be loaded as a modal
frame upon page load. One concrete example of this would be to have the user add
a profile upon registering.

Usage
-----
The way to accomplish the above example would be by adding a class="automodal"
to your node/add/profile link, then, upon registration, redirecting the user to
?q=user&automodal=node/add/profile

The code will look for any .automodal link that matches the path passed in the
URL, and will trigger a click on it.

To do
-----
* Add a preprocess function that will add automodal to all links on specific
pages (if enabled in the admin settings).
* Add visibility settings (similar to block visibility settings).
 
Dependencies
------------
This module depends on Automodal, which in turn depends on Modal Frame API,
which in its own turn depends on jQuery UI.

Credits
-------
* Developed by Victor Kareh (vkareh) - http://drupal.org/user/144520
* Sponsored by NBC Universal
